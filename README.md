## Gitlab (Début 15h30, Durée totale 1h30)

### Rendu

- Pour rendre les questions de cours, vous devez créer un projet Gitlab qui s'appelle `exam-gitlab-dec-2022`
- A l'intérieur du projet je dois trouver
  - Un fichier `NOM` avec votre nom dedans
  - Un fichier `NOM_PRENOM_questions_cours_gitlab.txt` qui contient les réponses aux questions de cours
- Vous m'ajouterez dans le projet en tant que `Mainteneur` de votre projet. Mon nom d'utilisateur est @tsaquet. 

- Pour la deuxième partie, il s'agit d'un autre dépôt à rendre, toutes les informations sont directement dans la consigne de l'exercice.

### Questions de cours (15 minutes - 5 pts)

Les questions sont à rendre au plus tard à 15h45 via un commit, dans le fichier `NOM_PRENOM_questions_cours_gitlab.txt` qui contient les questions (copiées ci-dessous) et les réponses. Chaque minute de retard vous coûte un point.
**Les réponses doivent tenir en une phrase.**  
**Vous devez écrire les réponses vous même, avec vos mots.**  

#### Git

- Qu'est-ce que Git ?

- A quoi sert la branche develop ?

- Qu'est-ce qu'un merge ?

- Qu'est-ce qu'un conflit ?

- Comment ignore-t-on les fichiers qu'on ne souhaite pas suivre avec git ?

#### Gitlab

- A quoi sert Gitlab ?

- Qu'est-ce que l'intégration continue (CI) ?

- Qu'est-ce que la livraison continue (CD) ?

- Qu'est-ce que le déploiement continu (CD) ?

- Qu'est-ce qu'un pipeline ?

- Qu'est-ce qu'un stage ?

- Qu'est-ce qu'un job ?

- Qu'est-ce qu'un runner ?

- Quel fichier permet de mettre en oeuvre la CI/CD dans un dépôt ?

- En quoi consiste une Merge Request ?

- Comment faire pour qu'un job se déclenche uniquement quand un autre est terminé ?

- Quelle est la procédure pour associer un runner à un projet Gitlab ?

- Quel mot clé permet de définir les commandes à exécuter sur le runner dans le fichier `.gitlab-ci.yml` ?

- Quels outils peuvent être utilisés à la place de Gitlab ? (Citez trois produits concurrents)

- Quel est le nom du fichier de configuration de Gitlab lorsque l'on fait une installation Omnibus ?

### Exercices Gitlab (1h15 - 15 pts)

L'examen se déroule intégralement sur https://gitlab.com.

Le rendu de cet exercice sera votre dépôt git en tant que tel. Pour ça, vous m'ajouterez dans Gitlab en tant que **"Mainteneur"** de votre projet. Mon nom d'utilisateur est @tsaquet. Ainsi j'aurais accès à tous vos commits et à tout votre travail. Je ne prends en compte que les commits qui auront eu lieu avant 17h !

La pertinence des commits et la clarté des messages sera prise en compte dans la notation. (Des commits qui concernent des éléments unitaires et qui sont bien commentés seront appréciés !)

Ajoutez un fichier NOM dans le dépôt qui contient vos nom et prénom afin que je puisse déterminer qui vous êtes (le pseudo gitlab n'aide pas toujours !)

L'objectif de cet exercice est de créer un pipeline complet qui va comporter les différentes étapes nécessaires pour livrer une image Docker en bonne et due forme et la mettre en production.

Nous allons pour ça utiliser le dépôt de l'utilitaire Whoami (que vous avez mis en oeuvre dans l'examen Docker). Sur Github, la CI/CD de ce projet en go est géré par Github Actions. Le but de l'examen est de la porter sur Gitlab CI.

Dans ce dépôt, beaucoup de bonnes pratiques de développement sont respectées :
- il y a le code source bien sûr
- mais aussi des tests
- du lint (controle de la syntaxe !)
- et une image Docker comme livraison
Nous allons reproduire tout cela dans Gitlab.

##### Etape 1 : Importer le projet Whoami dans Gitlab

_Objectif_ : Importer le projet https://github.com/traefik/whoami dans Gitlab. 

Pour ça, lorsque vous créez un nouveau projet, utilisez la fonction d'import **par URL**. Ne modifiez pas le nom (le projet doit garder le nom Whoami). N'oubliez pas de m'ajouter en **Mainteneur** une fois que c'est fait.

Le projet doit être **privé**

##### Etape 2 : Ajouter un runner au projet

_Objectif_ : Utilisez la commande qui convient pour Register un runner pour votre projet.

##### Etape 3 : Démarrer la CI

_Objectif_ : Ajouter un fichier `.gitlab-ci.yml` dans votre projet.
Ajouter un stage "check" et un job dans ce stage qui vérifie la syntaxe du code source, en anglais cette action s'appelle "to lint".

La commande à exécuter pour le lint du code est `make check` (vous pouvez trouver ce qu'elle fait dans le Makefile, ça peut vous indiquer quelle image utiliser !) 

Astuce : Comme vous allez le voir dans le `Makefile`, golangci-lint est un exécutable, il faut qu'il soit présent dans votre image pour que cela puisse fonctionner.
Lire la doc peut être une bonne piste : https://golangci-lint.run/usage/install/#other-ci, regardez du côté de l'image officielle.

##### Etape 4 : Stage build

_Objectif_ : Toujours dans le fichier .gitlab-ci.yml, ajoutez un stage appelé "build" et un job dans ce stage qui build le code source.
La commande à exécuter pour le lint du code est `make build` et est également issue du Makefile.

Astuce : Comme vous pouvez le voir dans le `Makefile` c'est du go, il faut donc une image qui connaisse le binaire go... Là aussi vous devriez trouver une image officielle qui devrait vous arranger !

Nous venons de builder le code mais nous voulons aussi builder l'image Docker correspondante. Dans le même stage "build", ajoutez un job appelé **docker-image** qui build l'image Docker et la pousse dans la registry locale au repo. Le **tag** de l'image doit être la variable $CI_COMMIT_REF_SLUG.

Pour ça aidez-vous de la documentation que vous pouvez trouver ici :
https://docs.gitlab.com/ee/user/packages/container_registry/#authenticate-by-using-gitlab-cicd

Ne paniquez pas et inspirez-vous des exemples !

##### Etape 5 : Stage check

_Objectif_ : Dans ce même fichier, ajoutez dans le stage "check"  un job qui lance les tests.
La commande à exécuter pour ceci est `make test` et est encore issue du Makefile :

```bash
test:
	go test -v -cover ./...
```

##### Etape 6 : Déploiement !

_Objectif_ : Déployez l'image Docker créée en 4. sur votre runner dans un stage de deploy uniquement appelé sur la branche master. Le conteneur s'appelle whoami.

Pour déployer, il vous suffit de run l'image via une commande script en suivant la documentation.

_Bonus_ : trouvez comment supprimer le conteneur précédent si on veut relancer le job !

Vérifiez sur votre poste de travail que le conteneur fonctionne bien (lire le README.md pour savoir comment faire)

##### Etape 7 : **Bonus**, on check après le déploiement !

_Objectif_ : Intégrez la commande de check qu'on vient de voir dans le déploiement !

Pour ça, il vous faudra ajouter curl dans le conteneur, vous pouvez le faire via les commandes script ou en modifiant l'image.
